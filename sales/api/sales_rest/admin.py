from django.contrib import admin
from .models import SalesRecord, SalesPerson, Customer

admin.site.register(SalesRecord)
admin.site.register(SalesPerson)
admin.site.register(Customer)


