from common.json import ModelEncoder
from .models import AutomobileVO, Customer, SalesPerson, SalesRecord



class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number"]



class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]



class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "sales_person",
        "customer",
        "automobile", 
        "price", 
        "id"
    ]
    encoders ={"sales_person": SalesPersonDetailEncoder(),}
    

    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin, "customer": o.customer.name}






