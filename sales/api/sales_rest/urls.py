from django.urls import path
from .views import api_list_customers, api_list_sales_records, api_list_sales_person

# from .views import 

urlpatterns = [
    path("sales_records/", api_list_sales_records, name="api_create_sales_record"),
    path("sales_person/", api_list_sales_person, name="api_create_sales_person"),
    path("customer/", api_list_customers, name="api_create_customer"),
    path("sales_person/<int:employee_number>/sales_records/", api_list_sales_records, name="api_list_sales_history"),
]