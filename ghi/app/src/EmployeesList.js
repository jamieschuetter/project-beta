import React from "react";


class EmployeesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            employees: []
        }
    }


    
    async componentDidMount() {
        let employees = []
        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const techResponse = await fetch(technicianUrl);
        if (techResponse.ok) {
            const techData = await techResponse.json();
            techData.technicians.forEach(object => {object.title = 'Technician'})
            employees = employees.concat(techData.technicians)
        }
        const salesUrl = 'http://localhost:8090/api/sales_person/'
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            salesData.sales_persons.forEach(object => {object.title = 'Salesperson'})
            employees = employees.concat(salesData.sales_persons)
        }
        let strAscending = [...employees].sort((a,b) => a.name.localeCompare(b.name))
        this.setState({employees: strAscending})
    }
    
    render() {
        return (
            <div>
                <h1 className="text-center mt-4 mb-5">Employees</h1>
                <table className="table table-striped mt-4">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee number</th>
                        <th>Title</th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.state.employees.map(employee => {
                            return (
                                <tr key={employee.employee_number} >
                                    <td>{employee.name}</td>
                                    <td>{employee.employee_number}</td>
                                    <td>{employee.title}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default EmployeesList;