import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentForm from './ServiceAppointmentForm';
import SalesRecordForm from './SalesRecordForm';
import ServiceAppointments from './ServiceAppointmentsList';
import ServiceHistory from './ServiceHistory';
import TechnicianForm from './TechnicianForm';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import CustomersList from './CustomersList';
import SalesRecordsList from './SalesRecordsList';
import SalesPersonsHistory from './SalesPersonsHistory';
import ManufacturerForm from './ManufacturerForm';
import ModelForm from './ModelForm';
import AutomobileForm from './AutomobileForm'
import ManufacturersList from './ManufacturersList';
import ModelsList from './ModelsList';
import AutomobilesList from './AutomobilesList';
import EmployeesList from './EmployeesList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="services">
            <Route index element={<ServiceAppointments />} />
            <Route path="history" element={<ServiceHistory />} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>
          <Route path="sales_records">
            <Route index element={<SalesRecordsList />} />
            <Route path="history" element={<SalesPersonsHistory />} />
            <Route path="new" element={<SalesRecordForm />} />
          </Route>
          <Route path="employees">
            <Route index element={<EmployeesList />} />
            <Route path="technicians">
              <Route path="new" element={<TechnicianForm />} />
            </Route>
            <Route path="sales_persons">
              <Route path="new" element={<SalesPersonForm />} />
            </Route>
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="inventory">
            <Route path="manufacturers" element={<ManufacturersList />} />
            <Route path="models" element={<ModelsList />} />
            <Route path="automobiles" element={<AutomobilesList />} />
            <Route path="new">
              <Route path="manufacturer" element={<ManufacturerForm />} />
              <Route path="model" element={<ModelForm />} />
              <Route path="automobile" element={<AutomobileForm />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
