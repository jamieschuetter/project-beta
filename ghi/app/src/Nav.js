import { NavLink } from 'react-router-dom';
import './index.css'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNzUycHQiIGhlaWdodD0iNzUycHQiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDc1MiA3NTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiA8ZyBmaWxsPSIjZmZmIj4KICA8cGF0aCBkPSJtMzEzLjAzIDE5MS4zNi0xMjguMTggNzR2NTcuNDAybDEyOC4xOC03NC4wMDQgMTI4LjE3IDc0LjAwNHYtNTcuNDAyeiIvPgogIDxwYXRoIGQ9Im00MzguOTggNTYxLjEzLTEyOC4xNy03My45OTZ2LTU3LjM5NWwxMjguMTcgNzQgMTI4LjE3LTc0djU3LjM5NXoiLz4KICA8cGF0aCBkPSJtMTg0Ljg2IDMzOC4xNyAxMjguMTgtNzQgNDkuNjk5IDI4LjY5NS0xMjguMTcgNzQuMDA0djE0OGwtNDkuNzA3LTI4LjY5OXoiLz4KICA8cGF0aCBkPSJtNTA0LjI2IDIyOC41OS0xMjguMTgtNzMuOTk2LTQ5LjcwNyAyOC42OTkgMTI4LjE3IDczLjk5NnYxNDhsNDkuNzExLTI4LjY5OXoiLz4KICA8cGF0aCBkPSJtMjQ4LjE4IDUyMy40MSAwLjAwMzkwNi0xNDggNDkuNzA3LTI4LjY5OXYxNDhsMTI4LjE3IDczLjk5Ni00OS43MDcgMjguNzAzeiIvPgogIDxwYXRoIGQ9Im01NjcuMTUgNDE0LjMydi0xNDhsLTQ5LjcwNy0yOC42OTl2MTQ4bC0xMjguMTcgNzQuMDA0IDQ5LjcwNyAyOC43MDd6Ii8+CiA8L2c+Cjwvc3ZnPgo=" 
            alt="" width="50" height="50"/>CarCar
        </NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" 
        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
        aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/sales_records">Sales Records</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales_records/history">Salesperson Record History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales_records/new">Create Sales Record</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Services
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/services">Service Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/services/history">Service History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/services/new">Create Appointment</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Inventory
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/inventory/automobiles">Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/inventory/models">Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/inventory/manufacturers">Manufacturers</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              New Inventory
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/inventory/new/automobile">Automobile</NavLink></li>
                <li><NavLink className="dropdown-item" to="/inventory/new/model">Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/inventory/new/manufacturer">Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Customers
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/customers/">Customers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/new">Create Customer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Employees
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/employees">Employees</NavLink></li>
                <li><NavLink className="dropdown-item" to="/employees/sales_persons/new">Create Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/employees/technicians/new">Create Technician</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
