import React from "react";

class SalesRecordsList extends React.Component {
    constructor(props) {
        super(props) 
        this.state = {
            sales: []
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/sales_records/'

            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                this.setState({sales: data.sales})
                }
            }
    
    

    render() {
        return (
            <div>
                <h1 className="text-center mt-5 mb-5">Sales Records</h1>
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Sales Person</th>
                    <th>Employee Number</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                            <td>{ sale.sales_person.name}</td>
                            <td>{ sale.sales_person.employee_number}</td>
                            <td>{ sale.customer}</td>
                            <td>{ sale.automobile}</td>
                            <td>{ new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(sale.price)}</td>
                            </tr>
                        );
                    })}
                </tbody>
                </table>
            </div>
        )
    }
}

export default SalesRecordsList

