import React from "react";


class AutomobilesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            autos: [],
            // sort: {
            //     by: '',
            //     order: 'descending',
            // },
        }
    }
    
    // handleSort() {

    // }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({autos: data.autos})
            // console.log(this.state.sort.order)
        };
    }

    render() {
        // let arrow = <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-caret-up-fill" viewBox="0 0 16 16">
        //             <path d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
        //             </svg>
        // if (this.state.sort.order === 'descending') {
        //     arrow = <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-caret-down-fill" viewBox="0 0 16 16">
        //             <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
        //             </svg>
        // }
        return (
            <div>
                <h1 className="text-center mt-4 mb-5">Automobiles</h1>
                <table className="table table-striped mt-4">
                    <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color
                            {/* <button className="btn" >
                                {arrow}
                            </button> */}
                        </th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.state.autos.map(auto => {
                            return (
                                <tr key={auto.id} >
                                    <td>{auto.vin}</td>
                                    <td>{auto.color}</td>
                                    <td>{auto.year}</td>
                                    <td>{auto.model.name}</td>
                                    <td>{auto.model.manufacturer.name}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AutomobilesList;