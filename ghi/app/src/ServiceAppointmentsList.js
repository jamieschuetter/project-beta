import React from 'react';

class ServiceAppointments extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointments: [],
        }
    }
    
    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointments/open/';
        
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({appointments: data.appointments})
        };
    }

    setAppointments(id) {
        let refreshList = this.state.appointments.filter(appointment => appointment.id !== id)
        this.setState({appointments: refreshList})
    }

    async cancelAppointment(id) {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {method: "delete"}
        const response = await fetch(url, fetchConfig)
        if(response.ok) {
            this.setAppointments(id);
        }
    }

    async finishAppointment(id) {
        const url = `http://localhost:8080/api/appointments/${id}/complete/`
        const fetchConfig = {method: "put"}
        const response = await fetch(url, fetchConfig)
        if(response.ok) {
            this.setAppointments(id);
        }
    }
    
    render() {
        
        
        return (
            <div>
                <h1 className="text-center mt-4 mb-5">Service appointments</h1>
                <table className="table table-striped mt-4">
                    <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>VIP</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            let vipTreatment = <td></td>
                            if (appointment.VIP_treatment) {
                                vipTreatment = 
                                <td>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-star-fill text-warning" viewBox="0 0 16 16">
                                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                    </svg>
                                </td>
                            }
                            return (
                                <tr key={appointment.id} >
                                    <td>{appointment.automobile_vin}</td>
                                    <td>{appointment.owner_name}</td>
                                    {vipTreatment}
                                    <td>{new Date(appointment.appointment_time).toLocaleDateString()}</td>
                                    <td>{new Date(appointment.appointment_time).toLocaleTimeString()}</td>
                                    <td>{appointment.technician.name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button onClick={() => this.cancelAppointment(appointment.id) }
                                        className="btn btn-danger btn-sm">Cancel</button>
                                        <button onClick={() => this.finishAppointment(appointment.id) }
                                        className="btn btn-success btn-sm">Finished</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default ServiceAppointments;