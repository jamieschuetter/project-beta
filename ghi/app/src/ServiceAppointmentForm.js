import React from 'react';

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            owner_name: '',
            appointment_time: '',
            automobile_vin: '',
            reason: '',
            technician: '',
            technicians: [],
            successCreate: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleInputChange(event) {
        const value = event.target.value;
        this.setState({[event.target.id]: value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.technicians;
        delete data.successCreate;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            // eslint-disable-next-line
            const newAppointment = await response.json();
            const cleared = {
                owner_name: '',
                appointment_time: '',
                automobile_vin: '',
                reason: '',
                technician: '',
                successCreate: true,
            };
            this.setState(cleared);
        }
    }
    
    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({technicians: data.technicians})
        };
    }

    render() {
        let successClassName = 'alert alert-success d-none mb-0 mt-5 text-center';
        let formClassName = '';
        if (this.state.successCreate) {
            successClassName = 'alert alert-success mb-0 mt-5 text-center';
            formClassName = 'd-none';
        }
        
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="text-center shadow p-4 mt-4">
                        <h1>Create a service appointment</h1>
                        <form className={formClassName} id="create-appointment-form" onSubmit={this.handleSubmit}>
                        <div className="form-floating mb-3">
                            <input 
                            onChange={this.handleInputChange} value={this.state.owner_name} 
                            placeholder="owner_name" required type="text" name="owner_name" id="owner_name" 
                            className="form-control"/>
                            <label htmlFor="owner_name">Owner's name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                            onChange={this.handleInputChange} value={this.state.appointment_time} 
                            placeholder="appointment_time" required type="datetime-local" name="appointment_time" id="appointment_time" 
                            className="form-control"/>
                            <label htmlFor="appointment_time">Appointment Date/Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                            onChange={this.handleInputChange} value={this.state.automobile_vin} 
                            placeholder="automobile_vin" required type="text" maxLength="17"
                            name="automobile_vin" id="automobile_vin" 
                            className="form-control"/>
                            <label htmlFor="automobile_vin">Automobile VIN</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="reason" className="form-label">Reason for appointment</label>
                            <textarea 
                            onChange={this.handleInputChange} value={this.state.reason} 
                            required type="text" name="reason" id="reason" 
                            className="form-control" rows="2"></textarea>
                        </div>
                        <div className="mb-3">
                            <select 
                            onChange={this.handleInputChange} value={this.state.technician} 
                            required name="technician" id="technician" className="form-select">
                            <option value="">Choose a technician</option>
                            {this.state.technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_number} value={technician.employee_number}>
                                            {technician.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-success">Create</button>
                        </form>
                        <div className={successClassName} id="success-message">
                            Appointment has been scheduled
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AppointmentForm;