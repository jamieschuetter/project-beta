import React from 'react';


class CustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            address: '',
            phone_number: '',
            createdCustomer: false
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const value = event.target.value
        this.setState({[event.target.id]: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.createdCustomer

    const CustomerUrl = 'http://localhost:8090/api/customer/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const response = await fetch(CustomerUrl, fetchConfig)
    if (response.ok) {
        // eslint-disable-next-line
        const newCustomer= await response.json()
        const cleared = {
            name: '',
            address: '',
            phone_number: '',
            createdCustomer: true
        }
        this.setState(cleared)
        }    
    }


    render() {

        let messageClasses = 'alert alert-success p-2 mt-5 d-none mb-0';
        if (this.state.createdCustomer) {
            messageClasses = 'alert alert-success mb-0';
        }
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="text-center shadow p-4 mt-4">
                <h1>Create a new customer</h1>
                <form onSubmit={this.handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Customer Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.phone_number} placeholder="Phone_number " required type="text" name="phone_number" id="phone_number" className="form-control"  />
                    <label htmlFor="phone_number">Phone Number</label>
                </div>
                <button className="btn btn-success">Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                    A new customer has been added.  Keep up the great work!
                </div>
            </div>
            </div>
        </div>
        );
    }
}

export default CustomerForm;