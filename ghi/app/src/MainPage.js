import React from "react"
import "./index.css"


function MainPage() {
  return (
    <div className="car">
      <div className="title">
        <div className="px-4 py-5 my-5 text-center">
          <h1 className="display-5 fw-bold">CarCar</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              The premiere solution for automobile dealership
              management!
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
