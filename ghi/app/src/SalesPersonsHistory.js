import React from "react";



class SalesPersonsHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_persons: [],
            sales: []
        }
    this.handleInputChange = this.handleInputChange.bind(this)
    }

    async handleInputChange(event) {
        const value = event.target.value
        this.setState({[event.target.id]: value})
        const sales_person_url = `http://localhost:8090/api/sales_person/${value}/sales_records`
        const response = await fetch(sales_person_url)
        if (response.ok) {
            const data = await response.json()
            this.setState({sales: data.sales})
        }
    }
    
    async componentDidMount() {

        const sales_personsUrl = 'http://localhost:8090/api/sales_person'
        const sales_persons_response = await fetch(sales_personsUrl)
        if (sales_persons_response.ok) {
            const sales_person_data = await sales_persons_response.json()
            this.setState({sales_persons: sales_person_data.sales_persons})
        }
        const salesUrl = `http://localhost:8090/api/sales_records/`
            const response = await fetch(salesUrl)
            if (response.ok) {
                const data = await response.json()
                this.setState({sales: data.sales})
            }
    }

    render() {
        return (
            <div>
                <h1 className="text-center mt-5 mb-5">Sales Person History</h1>
                <div className="mb-4" >
                    <select onChange={this.handleInputChange} value={this.state.sales_person} required name="name" id="name" className="form-select" >
                    <option value="">Choose a salesperson</option>
                    {this.state.sales_persons.map(employee => {
                        return (
                            <option key={employee.employee_number} value={employee.employee_number}>
                                {employee.name}
                            </option>
                        )
                    }
                        )}
                    </select>
                </div>
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Sales Person</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.sales.map(sale => {
            
                            return (
                                <tr key={sale.id}>
                                <td>{ sale.sales_person.name}</td>
                                <td>{ sale.customer}</td>
                                <td>{ sale.automobile}</td>
                                <td>{  new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(sale.price)}</td>
                                </tr>
                            );
                        
                    })}
                </tbody>
                </table>
            </div>
        )
    }
}

export default SalesPersonsHistory