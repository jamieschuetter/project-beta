import React from 'react';


class ServiceHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            appointments: [],
        };
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    }

    handleSearchChange(event) {
        const value = event.target.value;
        this.setState({search: value})
    }

    async handleSearch(event) {
        event.preventDefault();
        const vin = this.state.search;

        const vinUrl = `http://localhost:8080/api/vin/${vin}/appointments/`
        const response = await fetch(vinUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({appointments: data.appointments})
        }

    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointments/complete/'

        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            this.setState({appointments: data.appointments, search: ''})
        }
    }

    render () {
        return (
            <div>
                <h1 className="text-center mt-4 mb-5">Service history</h1>
                <form onSubmit={this.handleSearch} className="d-flex mt-2 mb-2">
                    <input className="form-control me-2" type="search" onChange={this.handleSearchChange} value={this.state.search}
                    placeholder="Search for VIN" aria-label="Search" />
                    <button className="btn btn-outline-secondary me-2" type="submit">Search</button>
                </form>
                <div className="text-center" >
                    <button className="btn btn-outline-secondary me-2" onClick={this.componentDidMount} type="submit">Clear Search</button>
                </div>
                <table className="table table-striped mt-4">
                    <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            return (
                                <tr key={appointment.id} >
                                    <td>{appointment.automobile_vin}</td>
                                    <td>{appointment.owner_name}</td>
                                    <td>{new Date(appointment.appointment_time).toLocaleDateString()}</td>
                                    <td>{new Date(appointment.appointment_time).toLocaleTimeString()}</td>
                                    <td>{appointment.technician.name}</td>
                                    <td>{appointment.reason}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default ServiceHistory;