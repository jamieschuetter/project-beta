import React from 'react';


class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            employee_number: '',
            createdEmployee: false
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const value = event.target.value
        this.setState({[event.target.id]: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.createdEmployee

    const SalesPersonUrl = 'http://localhost:8090/api/sales_person/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const response = await fetch(SalesPersonUrl, fetchConfig)
    if (response.ok) {
        // eslint-disable-next-line
        const newSalesPerson = await response.json()
        const cleared = {
            name: '',
            employee_number: '',
            createdSalesPerson: true
        }
        this.setState(cleared)
        }    
    }


    render() {

        let messageClasses = 'alert alert-success p-2 mt-5 d-none mb-0';
        if (this.state.createdSalesPerson) {
            messageClasses = 'alert alert-success mb-0';
        }
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="text-center shadow p-4 mt-4">
                <h1>Create a new sales person</h1>
                <form onSubmit={this.handleSubmit} id="create-sales-person-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Employee Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} value={this.state.employee_number} placeholder="Employee_number " required type="number" name="employee_number" id="employee_number" className="form-control" />
                    <label htmlFor="employee_number">Employee Number</label>
                </div>
                <button className="btn btn-success">Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                    A new sales person has been added to the team!
                </div>
            </div>
            </div>
        </div>
        );
    }
}

export default SalesPersonForm;