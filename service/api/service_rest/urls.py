from django.urls import path

from .views import (
    api_technicians,
    api_list_appointments,
    api_appointment,
    api_appointment_complete,
    api_open_appointments,
    api_complete_appointments,
)


urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("vin/<str:auto_vin>/appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/", api_list_appointments, name="api_create_appointment"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path("appointments/<int:pk>/complete/", api_appointment_complete, name="api_appointment_complete"),
    path("appointments/open/", api_open_appointments, name="api_open_appointments"),
    path("appointments/complete/", api_complete_appointments, name="api_complete_appointments")
]
