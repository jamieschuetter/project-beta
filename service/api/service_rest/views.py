from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .models import AutomobileVO, Technician, Appointment
from .encoders import TechnicianListEncoder, AppointmentListEncoder


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all().order_by('name')
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else: # POST
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, auto_vin=None):
    if request.method == "GET":
        if auto_vin is None:
            appointments = Appointment.objects.all().order_by('-id')
        else:
            appointments = Appointment.objects.filter(automobile_vin=auto_vin).order_by('-appointment_time')
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else: # POST
        try:
            content = json.loads(request.body)
            technician_number = content["technician"]
            technician = Technician.objects.get(employee_number=technician_number)
            content["technician"] = technician
            try:
                if AutomobileVO.objects.get(vin=content["automobile_vin"]):
                    content["VIP_treatment"] = True
            except:
                content["VIP_treatment"] = False
                
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 404
            return response

    
@require_http_methods(["GET", "DELETE"])
def api_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response
    
    
    if request.method == "GET":
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    else: # DELETE
        appointment.delete()
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )


@require_http_methods(["PUT"])
def api_appointment_complete(request, pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.complete()
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
    )

    
@require_http_methods(["GET"])
def api_open_appointments(request):
    appointments = Appointment.objects.filter(service_complete=False).order_by('appointment_time')
    return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )


@require_http_methods(["GET"])
def api_complete_appointments(request):
    appointments = Appointment.objects.filter(service_complete=True).order_by('-appointment_time')
    return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )