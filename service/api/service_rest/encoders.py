from common.json import ModelEncoder

from .models import AutomobileVO, Technician, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties =  [
        "owner_name",
        "appointment_time",
        "reason",
        "technician",
        "service_complete",
        "VIP_treatment",
        "automobile_vin",
        "id",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }